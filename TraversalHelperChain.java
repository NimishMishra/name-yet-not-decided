/*
 * This class writes the basic script to traverse the helper chain
 */
package blockchainusecase;

/**
 *
 * @author Nimish Mishra
 */
public class TraversalHelperChain {
    
    public static void printChainData(){
        
        
        
        CreateHelperChain.printChain();
    
    }
    
    public static void getAnswer(){
        // We need to call this function to invoke the getStatus() function call
        SeekerBlock unansweredBlock = Ladder.getStatus();
        printThisData(unansweredBlock);
        CreateHelperChain.addNewBlock();
        TraversalHelperChain.printChainData();
    }
    
    public static void printThisData(SeekerBlock unansweredBlock){
        System.out.println("Displaying data of unanswered block: ");
        System.out.println("Message: " + unansweredBlock.data);
    }
    
    public static void something(){
        
        int size = CreateSeekerChain.seekerchain.size();
        
        int i = 0;
        while(i < size){
            // Invoke getAnswer() for all unanswered questions
            if(!CreateSeekerChain.seekerchain.get(i).isAnswered){
                TraversalHelperChain.getAnswer();
                CreateSeekerChain.seekerchain.get(i).isAnswered = true;
            }
            else
                System.out.println("Everything answered");
            i++;
        }
    }
}
