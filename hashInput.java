/*
This code is inspired by 'https://www.baeldung.com/sha-256-hashing-java'

This uses the Sha256 algorithm to hash an input and return a result
 */
package blockchainusecase;
import java.security.*;

/**
 *
 * @author Nimish Mishra
 * 
 */
public class hashInput {
    
    public static String formCryptoMessage(String str){		
		try {
			MessageDigest message_digest = MessageDigest.getInstance("SHA-256");	        
			byte[] our_hash = message_digest.digest(str.getBytes("UTF-8"));	        
			StringBuffer hex = new StringBuffer(); 
                        // A StringBuffer to store the hexadecimal form of our_hash
			for (int i = 0; i < our_hash.length; i++) {
				String sample_hex = Integer.toHexString(0xff & our_hash[i]);
				if(sample_hex.length() == 1) 
                                    hex.append('0');
				hex.append(sample_hex);
			}
			return hex.toString();
		}
		catch(Exception e) {
			throw new RuntimeException("Some exception " + e + "occured");
		}
    }
    
}
