package blockchainusecase;

/*
 * @author Nimish Mishra
 */
public class Ladder {
    
    // Finds and gets from CreateSeekerChain a block whose question has not been answered
    public static SeekerBlock getStatus(){
        return CreateSeekerChain.setStatus(); // returns an unanswered Seeker Block
    }
}
