/*
 * This class writes code to create and manage the helper chain.
 */
package blockchainusecase;

/**
 *
 * @author Nimish Mishra
 */

import java.util.*;

public class CreateHelperChain {
    static ArrayList<HelperBlock> helperchain = new ArrayList<>(); //static because all objects must refer to the same list
    
    
    
    public static void addNewBlock(){
        // when triggered adds a new block to the end of the chain
        String message = getMessage(); //inputs the required message from the user
        String hash = findPreviousBlock();
        helperchain.add(new HelperBlock(message, hash));
    }
    
    public static String getMessage(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your message: ");
        return sc.nextLine();
    }
    
    public static String findPreviousBlock(){
        //finds the hash of the last block in the chain
        
        if(!helperchain.isEmpty()){
            return helperchain.get(helperchain.size() - 1).hash;
        }
        
        return "0000000000000";
    }
    
    public static void printChain(){
        int size = helperchain.size();
        int i = 0; HelperBlock thisBlock;
      
        while(i < size){
            thisBlock = helperchain.get(i);
            System.out.println("Hash of this block : " + thisBlock.hash);
            System.out.println("Data in this block: " + thisBlock.data);
            System.out.println("Timestamp of this block: " + thisBlock.timestamp);
            System.out.println("Hash of the previous block: " + thisBlock.previousHash);
            i++;
        }
    }
    
}
