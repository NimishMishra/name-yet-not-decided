/*
 * This class writes code to create and manage the seeker chain.
 */
package blockchainusecase;

/**
 * @author Nimish Mishra
 */

import java.util.*;

public class CreateSeekerChain {
    static ArrayList<SeekerBlock> seekerchain = new ArrayList<>(); //static because all objects must refer to the same list
    
    
    
    public static void addNewBlock(){
        // when triggered adds a new block to the end of the chain
        String message = getMessage(); //inputs the required message from the user
        String hash = findPreviousBlock();
        seekerchain.add(new SeekerBlock(message, hash));
    }
    
    public static String getMessage(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your message: ");
        return sc.nextLine();
    }
    
    public static String findPreviousBlock(){
        //finds the hash of the last block in the chain
        
        if(!seekerchain.isEmpty()){
            return seekerchain.get(seekerchain.size() - 1).hash;
        }
        
        return "0000000000000";
    }
    
    public static void printChain(){
        int size = seekerchain.size();
        int i = 0; SeekerBlock thisBlock;
      
        while(i < size){
            thisBlock = seekerchain.get(i);
            System.out.println("Hash of this block : " + thisBlock.hash);
            System.out.println("Data in this block: " + thisBlock.data);
            System.out.println("Timestamp of this block: " + thisBlock.timestamp);
            System.out.println("Hash of the previous block: " + thisBlock.previousHash);
            i++;
        }
    }
    
    public static SeekerBlock setStatus(){ 
            // returns a block that has not been answered
        int size = seekerchain.size();
        
        int i = 0; SeekerBlock thisBlock;
        
        while(i < size){
            thisBlock = seekerchain.get(i);
            if(!thisBlock.isAnswered)
                return thisBlock;
            i++;
        }
        
        return null;
    }
    
}
