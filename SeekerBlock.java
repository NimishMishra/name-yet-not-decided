package blockchainusecase;

import java.util.Date;
import java.util.HashMap;

/**
 * This file represents the basic architecture of the blocks to be used by the seekers.
 * @author Nimish Mishra
 */
public class SeekerBlock {
    // All are public because we need them to be accessed from different scripts
    public String data;
    public double timestamp;
    
    public String hash;
    public String previousHash;
    
    public boolean isAnswered;
    
    
    public SeekerBlock(String data, String previousHash){
        this.data = data;
        this.previousHash = previousHash;
        this.timestamp = new Date().getTime();
        this.hash =  getHash();
        isAnswered = false;
    }
    
    
    private String getHash(){
        return hashInput.formCryptoMessage(this.data + Double.toString(timestamp) + previousHash);
    }
}
